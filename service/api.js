const express = require('express')
const app = express()
const port = 3000
const bodyParser = require('body-parser')
const {sheet} = require('./sheets.js')

// render client from service, this is mean "Serv Static file"
app.use('/', express.static('./public/')) 
app.use(bodyParser.json())

app.post('/form', (req,res) => {
    const obj = req.body;
    sheet(obj, () => {
        res.send('success')
    });
});

//{ Date: new Date(), machineNum: 'machineNum', produceA: 'produceA', produceB: 'produceB', machineWasteA: 'machineWasteA', humanWasteA: 'humanWasteA', machineWasteB:'machineWasteB', humanWasteB:'humanWasteB', notes:'notes' }



app.listen(port, '0.0.0.0', () => console.log(`Example app listening at http://localhost:${port}`))