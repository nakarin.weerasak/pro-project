async function sheet(object, callback) {
  const { GoogleSpreadsheet } = require('google-spreadsheet');
  
  // spreadsheet key is the long id in the sheets URL
  const doc = new GoogleSpreadsheet('1-dKs_u9s2x8EI0Rx5XTFQJAEgkKsxR_qG1SPuZwrUKg');
  await doc.useServiceAccountAuth(require('./key.json'));
  await doc.loadInfo(); // loads document properties and worksheets
  // console.log(doc.title);
  await doc.updateProperties({ title: 'renamed doc' });
  
  const sheet = doc.sheetsByIndex[0]; // or use doc.sheetsById[id]
  // console.log(sheet.title);
  // console.log(sheet.rowCount);
  await sheet.loadCells('A1:E10'); // loads a range of cells
  // console.log(sheet.cellStats); // total cells, loaded, how many non-empty
  const a1 = sheet.getCell(0, 0); // access cells using a zero-based index
  const c6 = sheet.getCellByA1('C6'); // or A1 style notation
  // access everything about the cell
  // console.log(a1.value);
  // console.log(a1.formula);
  // console.log(a1.formattedValue);
  // // update the cell contents and formatting
  // a1.value = 45766;
  // c6.formula = '=A1';
  // a1.textFormat = { bold: true };
  // c6.note = 'This is a note!';
  await sheet.saveUpdatedCells(); // save all updates in one call
  const larryRow = await sheet.addRow(object);
  callback()
};

module.exports = {
  sheet
}